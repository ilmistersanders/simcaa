import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { translate } from 'react-i18next'
import { Grid } from 'semantic-ui-react'

import { connect } from 'react-redux'
import { saveTeams, saveRoles } from '../store/actions/AdministrationActions'
import { userFetchDataSuccess, userRealFetchDataSuccess } from '../store/actions/UserActions'

import AdminMenu from './components/Menu'
import RootModal from './containers/RootModal'
import RootComponent from './containers/RootComponent'
import Helper from './components/Helper'

import ReloginModal from '../HOC/ReloginModal'

class Admin extends Component {
  componentDidMount() {
    this.props.saveTeamsToStore()
    this.props.saveRolesToStore()
    if (Object.entries(this.props.oldUser).length === 0 && this.props.oldUser.constructor === Object) {
      this.props.saveCurrentUser(this.props.currentUser)
    }
  }

  returnToProject() {
    if (Object.entries(this.props.oldUser).length !== 0 && this.props.oldUser.constructor === Object) {
      if (this.props.oldUser.id === this.props.currentUser.id) {
        this.props.restoreUser(this.props.oldUser)
      }
    }
    this.props.history.push('/')
  }

  render() {
    return (
      <div>
          <AdminMenu returnToProject={this.returnToProject.bind(this)} />
          <Grid container verticalAlign="middle">
            <Grid.Row column={1} centered>
              <Grid.Column>
                <Route exact path="/admin" render={() => (<Redirect to="/admin/home" />)}/>
                <Route path="/admin/home" component={ReloginModal(Helper)}/>
                <Route path="/admin/overview" component={ReloginModal(RootComponent)} />
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <RootModal/>
      </div>
    );
  }
}

const mapStateToProps = (state)=>{
  return {
    currentUser: state.user,
    oldUser: state.userReal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveTeamsToStore: () => dispatch(saveTeams()),
    saveRolesToStore: () => dispatch(saveRoles()),
    saveCurrentUser: (user) => dispatch(userRealFetchDataSuccess(user)),
    restoreUser: (user) => dispatch(userFetchDataSuccess(user))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(Admin))
import React, { Component,Fragment } from 'react'
import {Loader, Dimmer} from 'semantic-ui-react'
import {connect} from 'react-redux'

import UsersTable from '../components/UsersTable'
import ErrorMessage from '../components/ErrorMessage'
import { openModalEditUserParams, saveUserTeams } from '../../store/actions/AdministrationActions'
import { impersonateUser } from '../../store/actions/UserActions'

class  ManageTeamTable extends Component {
  constructor(props) {
    super(props)
    this.state={
      error: false,
      loading: false
    }
  }

  componentDidMount() {
    this.props.getUser(this.props.curTeam)
  }
  
  remove(id){
    this.props.removeUser(id)
  }

  mod(id){
    this.props.modUser(id)
  }

  // Take control of another user
  takeControlUser(id) {
    this.props.impersonateUser(id)
  }

  // MAIN RENDER
  render(){
    if(!this.state.loading){
      return (
        <Fragment>
          <UsersTable
            users={this.props.user_teams}
            selectable={false}
            manageUsers={true}
            removeUser={this.remove.bind(this)}
            openEdit = {this.props.openModalEditUserParams.bind(this)}
            roles={this.props.roles}
            userRoleID={this.props.user.role_id}
            takeControlUser={this.takeControlUser.bind(this)}
            loggedUserID={this.props.user.id}
          />
          <ErrorMessage hidden={!this.state.error} header="Errore!" message="Errore nel caricare gli utenti, prova a caricare un'altra pagina."/>
        </Fragment>
      )
    }else{
      return(
        <Dimmer active inverted>
          <Loader content="Caricando...."/>
        </Dimmer>)
    }
  }
}
const mapStateToProps = (state)=>{
  return{
    user_teams: state.adminReducer.user_teams,
    curTeam: state.modalReducer.toTeam,
    user: state.user,
    roles: state.adminReducer.roles
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    openModalEditUserParams: (toUser) => dispatch(openModalEditUserParams(toUser)),
    getUser: (team_id) => dispatch(saveUserTeams(team_id)),
    impersonateUser: (user_id) => dispatch(impersonateUser(user_id))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ManageTeamTable)

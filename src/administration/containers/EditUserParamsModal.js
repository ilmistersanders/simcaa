import React, { Component } from 'react'
import { Modal, Button, Icon, Popup, Tab, Message } from 'semantic-ui-react'

import PositiveMessage from '../components/PositiveMessage'
import NegativeMessage from '../components/ErrorMessage'
import EditUserForm from '../components/EditUserForm'
import EditPermissionsForm from '../components/EditPermissionsForm'

import {apolloFetch} from '../../store/apolloFetchUtils'
import {connect} from 'react-redux'
import {saveUserTeams} from '../../store/actions/AdministrationActions'

import { translate } from 'react-i18next'
import { escapeQuotes } from '../../store/functionUtils'

class EditUserParamsModal extends Component {
  constructor(props){
    super(props)
    this.state={
      error: false,
      loading: false,
      updated: false,
      somethingModded: false,
      name: this.props.toUser.name,
      email: this.props.toUser.email,
      username: this.props.toUser.username,
      organization: this.props.toUser.organization,
      link_web: this.props.toUser.web_site,
      status: this.props.toUser.status,
      role_id: this.props.toUser.role_id,
      newPerms: '',
      roleModded: false,
      new_password: '',
      repeat_password: '',
      activeIndex: 0,
      permissions: []
    }
  }

  componentDidMount(){
    this.setState({permissions: this.props.toUser.user_permissions})
  }

  nameChange(e){
    this.setState({name: e.target.value, somethingModded: true})
  }
  emailChange(e){
    this.setState({email: e.target.value, somethingModded: true})
  }
  usernameChange(e){
    this.setState({username: e.target.value, somethingModded: true})
  }
  orgChange(e){
    this.setState({organization: e.target.value, somethingModded: true})
  }
  linkWebChange(e){
    this.setState({link_web: e.target.value, somethingModded: true, roleModded: true})
  }
  statusChange() {
    this.setState({status: 1-this.state.status, somethingModded: true})
  }

  roleChange(e,data){
    let newPerms = this.props.roles.find((role)=>{return role.id === data.value})
    this.setState({
      role_id: data.value,
      permissions: newPerms.role_permissions,
      roleModded: true,
      somethingModded: true})
  }
  repeatPassChange(data){
    this.setState({repeat_password: data})
  }
  newPassChange(data){
    this.setState({new_password: data, somethingModded: true})
  }
  modUser(){
    let query = ``,
        parameters = `
          id: ${this.props.toUser.user_id},
          name: "${this.state.name}",
          email: "${this.state.email}",
          user: "${this.state.username}",
          organization: "${this.state.organization}",
          link_web: "${this.state.link_web}",
          status: ${this.state.status},
          role_id: ${this.state.role_id},
          idstaff: ${this.props.toUser.idstaff},
          user_permissions: "${escapeQuotes(this.state.permissions)}"
        `

    if (this.state.new_password !== '' || this.state.repeat_password !== '') {
      if (this.state.new_password === this.state.repeat_password) {
        // Build query con cambio password
        query = `
        mutation modUser{
          updateCaaUser(${parameters}, new_password: "${this.state.new_password}"){
            id
          }
        }
        `
      } else {
        // Errore, password diverse nelle input
        this.setState({error: true})
      }
    } else {
      // Build query senza password change
      query = `
        mutation modUser{
          updateCaaUser(${parameters}){
            id
          }
        }
        `
    }
    this.setState({loading: true})
    apolloFetch({query})
      .then((data)=>{
        if(!data.hasOwnProperty("errors")){
          //no errors returned, user updated correctly
          this.setState({loading: false, error: false, updated: true})
          this.props.saveUserTeamsToStore(this.props.toTeam)
        }else{
          this.setState({loading: false, error: true, updated: false})
        }
      })
      .catch((error)=>{
        this.setState({loading: false, error: true, updated: false})
      })
  }
  handleTabChange(e,{activeIndex}){
    this.setState({activeIndex, somethingModded: false})
  }
  handleChange(moddedKey){
    // handler for permissions change
    let tmp = this.state.permissions
    Object.keys(this.state.permissions).forEach((key,index)=>{
      if(key === moddedKey){
        tmp[key] = !this.state.permissions[key]
        this.setState({permissions: tmp, somethingModded: true})
      }
    })
  }
  updatePermissions(){
    let query = `
    mutation modUser{
      updateCaaUser(
        id: ${this.props.toUser.user_id},
        user_permissions: "${escapeQuotes(this.state.permissions)}"
      ){
        id
      }
    }
    `
    this.setState({loading: true})
    apolloFetch({query})
      .then((data)=>{
        if(!data.hasOwnProperty("errors")){
          //no errors returned, permissions updated correctly
          this.setState({loading: false, error: false, updated: true})
          this.props.saveUserTeamsToStore(this.props.toTeam)
        }else{
          this.setState({loading: false, error: true, updated: false})
        }
      })
      .catch((error)=>{
        this.setState({loading: false, error: true, updated: false})
      })
  }
  handleMod(which){
    if(which === 0){
      //launch params update
      this.modUser()
    }else{
      //launch permissions update
      this.updatePermissions()
    }
  }
  closeMessage(which){
    switch (which) {
      case 1:
        this.setState({updated: false})
        break
      case 2:
        this.setState({error: false})
        break
    }
  }
  render(){
    const editParamsForm = <EditUserForm
      mode='admin'
      name={this.state.name}
      nameChange={()=>this.nameChange.bind(this)}
      email={this.state.email}
      emailChange={()=>this.emailChange.bind(this)}
      username={this.state.username}
      usernameChange={()=>this.usernameChange.bind(this)}
      organization = {this.state.organization}
      orgChange={()=>this.orgChange.bind(this)}
      link_web = {this.state.link_web}
      linkWebChange={()=>this.linkWebChange.bind(this)}
      status={this.state.status}
      statusChange={this.statusChange.bind(this)}
      roles = {this.props.roles}
      roleChange={()=>this.roleChange.bind(this)}
      repeatPassChange={this.repeatPassChange.bind(this)}
      newPassChange={this.newPassChange.bind(this)}
      role_id={this.props.toUser.role_id}
      loggedRole_id={this.props.loggedUser.role_id}
      self={this.props.loggedUser.id === this.props.toUser.user_id ? true : false}
    />

    let indexRole = this.props.roles.findIndex(role => role.id === this.props.toUser.role_id)
    const editPermissionsForm = <EditPermissionsForm
      originalPermission={JSON.parse(this.props.roles[indexRole].role_permissions)}
      permissions={this.state.permissions}
      handlePermissionChange={this.handleChange.bind(this)}
    />
    const panes=[
      {menuItem: 'Parametri', render: ()=><Tab.Pane>{editParamsForm}</Tab.Pane>},
      {menuItem: 'Permessi', render: ()=>{
        return this.props.loggedUser.id !== this.props.toUser.user_id ? <Tab.Pane>{editPermissionsForm}</Tab.Pane> :
        <Message negative content='Non puoi cambiare i tuoi permessi' style={{textAlign: 'center'}} />
      }}
    ]
    return(
      <Modal closeIcon="close" open={this.props.openModal} onClose={this.props.closeModal}>
        <Modal.Header>
          Modifica parametri di <b>{this.props.toUser.name}</b>
        <Popup trigger={<Icon name='help' style={{float: 'right'}}/>}
          content="Modifica dei parametri dell'utente, basta digitare il nuovo valore per uno o più parametri e poi cliccare su Modifica. "/>
        </Modal.Header>
        <Modal.Content>
          <Tab panes={panes} activeIndex={this.state.activeIndex} onTabChange={this.handleTabChange.bind(this)}/>
          <PositiveMessage header="Tutto ok!" message="Utente modificato correttamente :)" hidden={!this.state.updated} onDismiss={()=>this.closeMessage(1)}/>
          <NegativeMessage header="Errore!" message="Non è stato possibile modificare i parametri dell'utente" hidden={!this.state.error} onDismiss={()=>this.closeMessage(2)}/>
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={()=>this.props.closeModal()} loading={this.state.loading}>Chiudi</Button>
          <Button positive onClick={()=>this.handleMod(this.state.activeIndex)} loading={this.state.loading} disabled={!this.state.somethingModded}>Modifica</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
const mapStateToProps = (state)=>{
  return{
    roles: state.adminReducer.roles,
    toUser: state.modalReducer.toUser,
    toTeam: state.modalReducer.toTeam,
    loggedUser: state.user
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    saveUserTeamsToStore: (team_id)=> dispatch(saveUserTeams(team_id))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(translate('translations')(EditUserParamsModal))

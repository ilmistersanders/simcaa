import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'

class Can extends Component {
    render() {
        // Check for multiple permission check with 'and' and 'or'
        let controller, multiple = null
        if (this.props.and) {
            controller = 'and'
            multiple = this.props.permission[this.props.and]
        } else if (this.props.or) {
            controller = 'or'
            multiple = this.props.permission[this.props.or]
        }

        // MAIN RENDER RETURN
        return (
            <Fragment>
                {Permission(!this.props.not, this.props.permission[this.props.perform], controller, multiple) ? this.props.children : null}
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        permission: state.user.user_permissions,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

Can.propTypes = {
    children: PropTypes.element.isRequired,
    perform: PropTypes.string.isRequired,
    not: PropTypes.bool,
    and: PropTypes.string,
    or: PropTypes.string,
}

Can.defaultProps = {
    not: false
}

export default connect(mapStateToProps, mapDispatchToProps)(Can)

export function Permission(checker, action, controller = null, multiple = null) {
    if (controller !== null && controller === 'and') {
        return action === checker && multiple === checker ? true : false
    } else if (controller !== null && controller === 'or') {
        return action === checker || multiple === checker ? true : false
    } else {
        return action === checker ? true : false
    }
}

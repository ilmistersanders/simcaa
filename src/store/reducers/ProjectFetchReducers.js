import { FETCH_PROJECT_DATA,FETCH_PROJECT_DATA_TOTAL, FETCH_PROJECT_DATA_ERROR, FETCH_PROJECT_DATA_LOADING } from '../constants'

export function projectDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_PROJECT_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function projectDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_PROJECT_DATA_LOADING:
            return action.isLoading
        default:
            return state
    }
}

export function projectData(state = [], action) {
    switch (action.type) {
        case FETCH_PROJECT_DATA:
            return action.project
        default:
            return state
    }
}

export function projectDataTotal(state = 0, action) {
    switch (action.type) {
        case FETCH_PROJECT_DATA_TOTAL:
            return action.total
        default:
            return state
    }
}

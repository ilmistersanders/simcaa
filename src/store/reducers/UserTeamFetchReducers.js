import { FETCH_USERTEAM_DATA, FETCH_MEMBERTEAM_DATA, FETCH_USERTEAM_DATA_ERROR, FETCH_USERTEAM_DATA_LOADING, FETCH_USERTEAM_DATA_COMPLETED } from '../constants'

export function userTeamDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_USERTEAM_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function userTeamDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_USERTEAM_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function userTeamDataIsCompleted(state = false, action) {
    switch (action.type) {
        case FETCH_USERTEAM_DATA_COMPLETED:
            return action.isCompleted

        default:
            return state
    }
}

export function userTeamData(state = [], action) {
    switch (action.type) {
        case FETCH_USERTEAM_DATA:
            return action.team

        default:
            return state
    }
}

export function memberTeamData(state = [], action) {
    switch (action.type) {
        case FETCH_MEMBERTEAM_DATA:
            return action.members

        default:
            return state
    }
}

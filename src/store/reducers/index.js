import { combineReducers } from 'redux'
import { user, userReal, userToHome, userHasErrored, userIsLoading } from './UserReducers'
import { jwt, jwtHasErrored, jwtIsLoading, jwtExpiredOrInvalid } from './JWTReducers'
import { projectData,projectDataTotal, projectDataHasErrored, projectDataIsLoading } from './ProjectFetchReducers'
import { profileData, profileDataHasErrored, profileDataIsLoading } from './ProfileFetchReducers'
import { layoutData, layoutDataHasErrored, layoutDataIsLoading } from './LayoutFetchReducers'
import { classData, classDataHasErrored, classDataIsLoading } from './ClassFetchReducers'
import { styleData, styleDataHasErrored, styleDataIsLoading } from './StyleFetchReducers'
import { symstyleData, symstyleDataHasErrored, symstyleDataIsLoading } from './SymStyleFetchReducers'
import { filterMode, filterValue, projectPaginationPage } from './FilterReducers'
import { userTeamData, memberTeamData, userTeamDataHasErrored, userTeamDataIsLoading, userTeamDataIsCompleted } from './UserTeamFetchReducers'
import { chapterData, chapterContentData, chapterRowsData,chapterTypoData, chapterDataHasErrored, chapterDataIsLoading } from './ChapterFetchReducers'
import { selectedImgSize } from './TypoReducers'
import { CardUINavbarCard, toggleVisibilityImgAlt, selectedProjectID, CardUIsaveIsCompleted, CardUIsaveHasErrored, CardUIsaveIsLoading, CardUIsearchIsLoading, CardUIcopyPaste } from './CardUIReducers'
import { watermarkData, watermarkDataHasErrored, watermarkDataIsLoading } from './WatermarkFetchReducers'
import { modalReducer, adminReducer } from './AdministrationReducers'

const appReducer = combineReducers({
    user,
    userReal,
    userToHome,
    userHasErrored,
    userIsLoading,

    jwt,
    jwtHasErrored,
    jwtIsLoading,
    jwtExpiredOrInvalid,

    projectData,
    projectDataTotal,
    projectDataHasErrored,
    projectDataIsLoading,

    profileData,
    profileDataHasErrored,
    profileDataIsLoading,

    layoutData,
    layoutDataHasErrored,
    layoutDataIsLoading,

    classData,
    classDataHasErrored,
    classDataIsLoading,

    styleData,
    styleDataHasErrored,
    styleDataIsLoading,

    symstyleData,
    symstyleDataHasErrored,
    symstyleDataIsLoading,

    filterMode,
    filterValue,
    projectPaginationPage,

    userTeamData,
    memberTeamData,
    userTeamDataHasErrored,
    userTeamDataIsLoading,
    userTeamDataIsCompleted,

    chapterData,
    chapterContentData,
    chapterRowsData,
    chapterTypoData,
    chapterDataHasErrored,
    chapterDataIsLoading,

    selectedImgSize,

    CardUINavbarCard,
    toggleVisibilityImgAlt,
    selectedProjectID,
    CardUIsaveIsCompleted,
    CardUIsaveHasErrored,
    CardUIsaveIsLoading,
    CardUIsearchIsLoading,
    CardUIcopyPaste,

    watermarkData,
    watermarkDataHasErrored,
    watermarkDataIsLoading,

    modalReducer,
    adminReducer
})

const rootReducer = (state, action) => {
    if (action.type === 'RESET_STORE') {
      state = undefined
    }
  
    return appReducer(state, action)
}

export default rootReducer
import { CARDUI_NAVBARCARD,
    CARDUI_VISIBILITY_IMGALT,
    CARDUI_SELECTED_PROJECT,
    CARDUI_SAVE_PROJECT_COMPLETED,
    CARDUI_SAVE_PROJECT_ERROR,
    CARDUI_SEARCH_ISLOADING,
    CARDUI_COPYPASTE,
    CARDUI_SAVE_PROJECT_LOADING } from '../constants'

export function CardUINavbarCard(state = {id: 0, lemma: 'simcaa', imgAlt: []}, action) {
    switch (action.type) {
        case CARDUI_NAVBARCARD:
            let updatedNavCard = Object.assign({}, action.navbarCard)
            return updatedNavCard
        default:
            return state
    }
}

export function toggleVisibilityImgAlt(state = false, action) {
    switch (action.type) {
        case CARDUI_VISIBILITY_IMGALT:
            return action.visibilityImgAlt
        default:
            return state
    }
}

export function selectedProjectID(state = -1, action) {
    switch (action.type) {
        case CARDUI_SELECTED_PROJECT:
            return action.projectId
        default:
            return state
    }
}

export function CardUIsaveIsCompleted(state = false, action) {
    switch (action.type) {
        case CARDUI_SAVE_PROJECT_COMPLETED:
            return action.isCompleted
        default:
            return state
    }
}

export function CardUIsaveHasErrored(state = false, action) {
    switch (action.type) {
        case CARDUI_SAVE_PROJECT_ERROR:
            return action.hasErrored
        default:
            return state
    }
}

export function CardUIsaveIsLoading(state = false, action) {
    switch (action.type) {
        case CARDUI_SAVE_PROJECT_LOADING:
            return action.isLoading
        default:
            return state
    }
}

export function CardUIsearchIsLoading(state = false, action) {
    switch (action.type) {
        case CARDUI_SEARCH_ISLOADING:
            return action.isLoading
        default:
            return state
    }
}

export function CardUIcopyPaste(state = {}, action) {
    switch (action.type) {
        case CARDUI_COPYPASTE:
            return action.card
        default:
            return state
    }
}

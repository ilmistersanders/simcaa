import { projectDataHasErrored, projectDataIsLoading } from './actions/ProjectFetchActions'
import { profileDataHasErrored, profileDataIsLoading } from './actions/ProfileFetchActions'
import { layoutDataHasErrored, layoutDataIsLoading } from './actions/LayoutFetchActions'
import { chapterDataHasErrored, chapterDataIsLoading } from './actions/ChapterFetchActions'

import store from './configureStore'

// Reset data error and loading flag in store
export function resetDataFlag(data) {
    let value
    data ? value = data : value = false
    store.dispatch(projectDataHasErrored(value))
    store.dispatch(projectDataIsLoading(value))

    store.dispatch(profileDataHasErrored(value))
    store.dispatch(profileDataIsLoading(value))

    store.dispatch(chapterDataHasErrored(value))
    store.dispatch(chapterDataIsLoading(value))

    store.dispatch(layoutDataHasErrored(value))
    store.dispatch(layoutDataIsLoading(value))
}

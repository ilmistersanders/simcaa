import { FETCH_CHAPTER_DATA, FETCH_CHAPTER_DATA_CONTENT, FETCH_CHAPTER_DATA_ROW, FETCH_CHAPTER_DATA_TYPO, FETCH_CHAPTER_DATA_ERROR, FETCH_CHAPTER_DATA_LOADING } from '../constants'
import { CardUINavbarCard } from './CardUIActions'
import { setExpandAll, setLineSpacing } from '../functionUtils'
import { apolloFetchNoAuth, apolloFetch } from '../apolloFetchUtils'

export function chapterDataFetchData(fetchQuery = null, limit = 100, page = 1, idProject, content = false) {
    return (dispatch, getState) => {
        dispatch(chapterDataIsLoading(true))
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query currentChapthers {
                chapters(caa_project_id: ${idProject}, limit: ${limit}, page: ${page}) {
                    data {
                        id
                        chapt_title
                        chapt_user_block
                    }
                }
            }
            `
        }
        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(chapterDataIsLoading(false))
                if (data.data.chapters.error) {
                    dispatch(chapterDataHasErrored(true))
                } else {
                    let initialRowsArray = [{times: 1, type: 'none', row: 0}]
                    if (content) {
                        let chapt_content = data.data.chapters.data[0].chapt_content
                        let contentData = chapt_content.length === 0 ? '' : JSON.parse(chapt_content)
                        if(Object.keys(contentData).length !== 0 && typeof(contentData) === 'object') {
                            dispatch(chapterContentDataFetchDataSuccess(contentData))
                            dispatch(chapterRowsDataFetchDataSuccess(data.data.chapters.data[0].chapt_row === '' ? initialRowsArray : JSON.parse(data.data.chapters.data[0].chapt_row)))
                            setTimeout(() => {
                                dispatch(CardUINavbarCard(0))

                                let indexProject = getState().projectData.findIndex(x => x.proj_id === getState().selectedProjectID)
                                let margins = JSON.parse(getState().projectData[indexProject].proj_profile).layout.layout_margins
                                setLineSpacing(margins)

                                setExpandAll(getState().chapterContentData.slice())
                            }, 0)
                        } else {
                            contentData = [{id: 0,
                                    lemma: 'simcaa',
                                    lemmaPrevious: 'simcaa',
                                    img: 'simcaa.png',
                                    sinonimi: 0,
                                    imgAlt: [{voice_human: 'simcaa',voice_master: 'simcaa', img: 'simcaa.png'}],
                                    lock: 'unlock',
                                    codClass: 'Altro',
                                    complex: '0',
                                    row: 0,
                                }]
                            dispatch(chapterContentDataFetchDataSuccess(contentData))
                            dispatch(chapterRowsDataFetchDataSuccess(initialRowsArray))
                        }
                        dispatch(chapterTypoDataFetchDataSuccess(JSON.parse(data.data.chapters.data[0].chapt_typo)))
                    } else {
                        dispatch(chapterDataFetchDataSuccess(data.data.chapters.data))
                    }

                    dispatch(chapterDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(chapterDataIsLoading(false))
                dispatch(chapterDataHasErrored(true))
            })
    }
}

export function chapterDataCreateData(mutationQuery, projectId) {
    return (dispatch, getState) => {

        dispatch(chapterDataIsLoading(true))
        let query = mutationQuery

        apolloFetch({ query })
            .then((data) => {
                dispatch(chapterDataIsLoading(false))
                if (data.data.createCaaChapter.error) {
                    dispatch(chapterDataHasErrored(true))
                } else {
                    dispatch(chapterDataHasErrored(false))
                    dispatch(chapterDataFetchData(null, 30, 1, projectId))
                }
            })
            .catch((error) => {
                dispatch(chapterDataIsLoading(false))
                dispatch(chapterDataHasErrored(true))
            })
    }
}

export function chapterDataUpdateData(mutationQuery, projectId = null) {
    return (dispatch, getState) => {

        dispatch(chapterDataIsLoading(true))
        let query = mutationQuery

        apolloFetch({ query })
            .then((data) => {
                dispatch(chapterDataIsLoading(false))
                if (data.data.updateCaaChapter.error) {
                    dispatch(chapterDataHasErrored(true))
                } else {
                    dispatch(chapterDataHasErrored(false))
                    if (projectId) {
                        dispatch(chapterDataFetchData(null, 30, 1, projectId))
                    }
                }
            })
            .catch((error) => {
                dispatch(chapterDataIsLoading(false))
                dispatch(chapterDataHasErrored(true))
            })
    }
}

export function chapterDataDeleteData(id, projectId) {
    return (dispatch, getState) => {

        dispatch(chapterDataIsLoading(true))
        let query = `
        mutation DeleteChapter {
            deleteCaaChapter(id: ${id}) {
                id
            }
        }
        `
        apolloFetch({ query })
            .then((data) => {
                dispatch(chapterDataIsLoading(false))
                if (data.data.deleteCaaChapter.error) {
                    dispatch(chapterDataHasErrored(true))
                } else {
                    dispatch(chapterDataHasErrored(false))
                    dispatch(chapterDataFetchData(null, 30, 1, projectId))
                }
            })
            .catch((error) => {
                dispatch(chapterDataIsLoading(false))
                dispatch(chapterDataHasErrored(true))
            })
    }
}

export function duplicateChapter(chapter_id) {
    return (dispatch, getState) => {
        let proj_id = getState().selectedProjectID
        let data = JSON.stringify({id: chapter_id})
        fetch(window.env.RestApiDuplicateChapter, {
                method: 'POST',
                body: data,
                headers:{
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                if (!isNaN(data)) {
                    dispatch(chapterDataFetchData(null, 30, 1, proj_id))
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }
}

export function chapterDataHasErrored(bool) {
    return {
        type: FETCH_CHAPTER_DATA_ERROR,
        hasErrored: bool
    }
}

export function chapterDataIsLoading(bool) {
    return {
        type: FETCH_CHAPTER_DATA_LOADING,
        isLoading: bool
    }
}

export function chapterDataFetchDataSuccess(chapter) {
    return {
        type: FETCH_CHAPTER_DATA,
        chapter
    }
}

export function chapterContentDataFetchDataSuccess(content) {
    return {
        type: FETCH_CHAPTER_DATA_CONTENT,
        content
    }
}

export function chapterRowsDataFetchDataSuccess(rows) {
    return {
        type: FETCH_CHAPTER_DATA_ROW,
        rows
    }
}

export function chapterTypoDataFetchDataSuccess(typo) {
    return {
        type: FETCH_CHAPTER_DATA_TYPO,
        typo
    }
}

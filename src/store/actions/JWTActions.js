import { FETCH_JWT_DATA, FETCH_JWT_DATA_ERROR, FETCH_JWT_DATA_LOADING, JWT_EXPIRED_OR_INVALID } from '../constants'
import { userFetchData } from './UserActions'

export function jwtFetchData(url, user, password, reLogin = false) {
    return (dispatch, getState) => {
        dispatch(jwtIsLoading(true))
        dispatch(jwtHasErrored(false))
        let data = JSON.stringify({user: user, password: password})
        fetch(url, {
                method: 'POST',
                body: data,
                headers:{
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(jwtIsLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((jwt) => dispatch(jwtFetchDataSuccess(jwt)))
            .then(() => {
                if (!reLogin) {
                    dispatch(jwtExpiredOrInvalid(false))
                }
            })
            .then(() => {
                dispatch(userFetchData(reLogin))
            })
            .catch(() => dispatch(jwtHasErrored(true)))
    }
}

export function jwtHasErrored(bool) {
    return {
        type: FETCH_JWT_DATA_ERROR,
        hasErrored: bool
    }
}

export function jwtIsLoading(bool) {
    return {
        type: FETCH_JWT_DATA_LOADING,
        isLoading: bool
    }
}

export function jwtExpiredOrInvalid(jwtError) {
    return {
        type: JWT_EXPIRED_OR_INVALID,
        jwtError
    }
}

export function jwtFetchDataSuccess(jwt) {
    return {
        type: FETCH_JWT_DATA,
        jwt
    }
}

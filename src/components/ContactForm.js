import React, { Component, Fragment } from 'react'
import { Modal, Button, Form, Message, Header } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import { apolloFetchNoAuth } from '../store/apolloFetchUtils'
import { escapeQuotes, validMail } from '../store/functionUtils'
import Captcha from './Captcha'

class ContactForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            mail: '',
            name: '',
            user: '',
            region: '',
            mailuser: '',
            message: '',
            captcha: false,
            loading: false,
            error: {hidden: true, text: ''},
            returnMessage: {hidden: true, text: ''},
            userMailCheck: {user: null, mail: null}
        }
    }

    // Handle open/close of the modal
    openCloseModal() {
        if (this.state.open === true) {
            this.setState({
                open: !this.state.open,
                mail: "",
                name: "",
                message: "",
                mailuser: "",
                user: "",
                region: "",
                loading: false,
                error: { hidden: true, text: "" },
                returnMessage: { hidden: true, text: "" }
            })
        } else {
            this.setState({open: !this.state.open})
        }
    }

    // Handle onChange of the form
    handleInputForm(type, e) {
        this.setState({[type]: e.target.value})
    }

    // Check the data before send the mail to the server
    checkData() {
        if (this.props.type === 'reset_password') {
            if (this.state.mailuser === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_MAILUSER")}})
            } else if (this.state.captcha === false) {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_CAPTCHA")}})
            } else {
                let formData = new FormData()
                formData.append('data', this.state.mailuser)
                formData.append('type', 'reset_password')
                formData.append('tmppassword', Math.random().toString(36).slice(-8))
               
                this.sendMail(formData)
            }
        } else if (this.props.type === 'request_account') {
            if (this.state.name === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_NAME")}})
            } else if (this.state.mail === '' || !validMail(this.state.mail)) {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_MAIL")}})
            } else if (this.state.message === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_MESSAGE")}})
            } else if (this.state.user === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_USER")}})
            } else if (this.state.region === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_REGION")}})
            } else if (this.state.captcha === false) {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_CAPTCHA")}})
            } else {
                this.checkUserMail()
            }
        } else {
            if (this.state.name === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_NAME")}})
            } else if (this.state.mail === '' || !validMail(this.state.mail)) {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_MAIL")}})
            } else if (this.state.message === '') {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_MESSAGE")}})
            } else if (this.state.captcha === false) {
                this.setState({error: {hidden: false, text: this.props.t("ERR_CONTACT_CAPTCHA")}})
            } else {
                let formData = new FormData()
                formData.append('name', this.state.name)
                formData.append('mail', this.state.mail)
                formData.append('mail_message', this.state.message)
                formData.append('type', 'request_support')

                this.sendMail(formData)
            }
        }
    }

    // Check username and email
    checkUserMail() {
        this.setLoading(true)
        let localUserMailCheck = Object.assign({}, this.state.userMailCheck)
        let checkUser = `
            query checkUser {
                caa_users(user: "${escapeQuotes(this.state.user)}") {
                    total
                    data {
                        id
                        user
                    }
                }
            }
        `
        
        let checkMail = `
            query checkMail {
                caa_users(email: "${escapeQuotes(this.state.mail)}") {
                    total
                    data {
                        id
                        user
                    }
                }
            }
        `
        apolloFetchNoAuth({query: checkUser})
            .then(data => {
                if (data.data.caa_users.total > 0) {
                    localUserMailCheck.user = true
                    this.setState({userMailCheck: localUserMailCheck,
                        error: {hidden: false, text: this.props.t("ERR_CONTACT_DUPLICATEUSER")}
                    })
                } else {
                    if (localUserMailCheck.user === true || localUserMailCheck.user === null) {
                        localUserMailCheck.user = false
                        this.setState({userMailCheck: localUserMailCheck, loading: false}, () => this.requestAccountMailSend())
                    } else {
                        this.setLoading(false)
                    }
                }
            })
            .catch(error => {
                this.setLoading(false)
                console.error(error)
            })
        apolloFetchNoAuth({query: checkMail})
            .then(data => {
                if (data.data.caa_users.total > 0) {
                    localUserMailCheck.mail = true
                    this.setState({userMailCheck: localUserMailCheck,
                        error: {hidden: false, text: this.props.t("ERR_CONTACT_DUPLICATEMAIL")}
                    })
                } else {
                    if (localUserMailCheck.mail === true || localUserMailCheck.mail === null) {
                        localUserMailCheck.mail = false
                        this.setState({userMailCheck: localUserMailCheck, loading: false}, () => this.requestAccountMailSend())
                    } else {
                        this.setLoading(false)
                    }
                }
            })
            .catch(error => {
                this.setLoading(false)
                console.error(error)
            })
    }

    // Check the data and send mail for the account request
    requestAccountMailSend() {
        if (Object.values(this.state.userMailCheck).every(item => item === false)) {
            let message = `
                username: ${this.state.user}
                mail: ${this.state.mail}
                name: ${this.state.name}
                region: ${this.state.region}
                message: ${this.state.message}
            `
            let formData = new FormData()
            formData.append('name', this.state.name)
            formData.append('mail', this.state.mail)
            formData.append('mail_message', message)
            formData.append('type', 'request_account')
            formData.append('account', this.props.account)

            this.sendMail(formData)
        }
    }
    
    // Send mail to the server
    sendMail(body) {
        this.setLoading(true)
        this.setState({error: {hidden: true, text: ''}}, () => {
            fetch(window.env.GraphQLMail, {
                body,
                method: "post"
            })
            .then(response => response.json())
            .then(data => {
                if (data.code === 200) {
                    if (this.props.type === 'request_account') {
                        this.setState({userMailCheck: {user: null, mail: null}}, () => {
                            let typerequest
                            if (this.props.account === 'privato') {
                                typerequest = 1
                            } else if (this.props.account === 'scuole') {
                                typerequest = 2
                            } else {
                                typerequest = 3
                            }
                            let query = `
                            mutation LogMail {
                                createCaaLogRequest(
                                    log_ip: "${data.data}",
                                    log_email: "${escapeQuotes(this.state.mail)}",
                                    log_username: "${escapeQuotes(this.state.user)}",
                                    log_region: "${escapeQuotes(this.state.region)}",
                                    log_typerequest: ${typerequest},
                                    log_messagerequest: "${escapeQuotes(this.state.message)}",
                                    log_disclaimer: "",
                                    log_statusrequest: 0,
                                    log_temppassword: ""
                                ) {
                                    id
                                }
                            }
                            `
                            apolloFetchNoAuth({query})
                                .then(data => {
                                    this.setState({returnMessage: {hidden: false, text: this.props.t("CTCT_MSG_EMAILOK")}}, () => {
                                        setTimeout(() => {
                                            this.openCloseModal()
                                        }, 2000)
                                    })
                                })
                                .catch(error => {
                                    this.setLoading(false)
                                    console.error(error)
                                })
                        })
                    } else {
                        this.setState({returnMessage: {hidden: false, text: this.props.t("CTCT_MSG_EMAILOK")}}, () => {
                            setTimeout(() => {
                                this.openCloseModal()
                            }, 2000)
                        })
                    }
                }
                if (data.code === 250) {
                    this.setState({error: {hidden: false, text: this.props.t("CTCT_MSG_EMAILERROR")}, loading: false})
                }
            })
        })
    }

    // Pass the captcha
    validationCaptchaPassed() {
        this.setState({captcha: true})
    }

    // Set the loading button
    setLoading(value = null) {
        if (value) {
            this.setState({loading: value})
        } else {
            this.setState({loading: !this.state.loading})
        }
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        let triggerModal, formBody
        if (this.props.type === 'reset_password') {
            triggerModal = (
              <Header
                size='small'
                onClick={this.openCloseModal.bind(this)}
                className="icon-pointer"
                disabled={window.env.demoMode}
              >
                Password dimentaicata?
              </Header>
            );
            formBody = <Form.Input required fluid label={t("CTCT_LBL_MAILUSER")} placeholder={t("CTCT_LBL_MAILUSER")}
                value={this.state.mailuser}
                onChange={this.handleInputForm.bind(this, 'mailuser')}
            />
        } else if (this.props.url) {
            triggerModal = <p className='similar-link' onClick={this.openCloseModal.bind(this)}>{this.props.url}</p>
            formBody = <Fragment>
                <Form.Group widths='equal'>
                    <Form.Input required fluid label={t("CTCT_LBL_CONTACTNAME")} placeholder={t("CTCT_LBL_CONTACTNAME")}
                        value={this.state.name}
                        onChange={this.handleInputForm.bind(this, 'name')}
                    />
                    <Form.Input required fluid label={t("CTCT_LBL_CONTACTEMAIL")} placeholder={t("CTCT_LBL_CONTACTEMAIL")}
                        value={this.state.mail}
                        onChange={this.handleInputForm.bind(this, 'mail')}
                        type='email'
                    />
                </Form.Group>
                <Form.TextArea required label={t("CTCT_LBL_CONTACTMESSAGE")} placeholder={t("CTCT_LBL_CONTACTMESSAGE") + '...'}
                    value={this.state.message}
                    onChange={this.handleInputForm.bind(this, 'message')}
                />
            </Fragment>
        } else if (this.props.type === 'request_account') {
            triggerModal = <Button
                color={this.props.color}
                style={this.props.style}
                disabled={window.env.demoMode}
                onClick={this.openCloseModal.bind(this)}
            >
                {t("CTCT_BTN_CONTACT")}
            </Button>
            formBody = <Fragment>
                <Form.Group widths='equal'>
                    <Form.Input required fluid label={t("CTCT_LBL_CONTACTNAME")} placeholder={t("CTCT_LBL_CONTACTNAME")}
                        value={this.state.name}
                        onChange={this.handleInputForm.bind(this, 'name')}
                    />
                    <Form.Input required fluid label={t("CTCT_LBL_CONTACTREGION")} placeholder={t("CTCT_LBL_CONTACTREGION")}
                        value={this.state.region}
                        onChange={this.handleInputForm.bind(this, 'region')}
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input required fluid label={t("CTCT_LBL_CONTACTUSER")} placeholder={t("CTCT_LBL_CONTACTUSER")}
                        value={this.state.user}
                        onChange={this.handleInputForm.bind(this, 'user')}
                    />
                    <Form.Input required fluid label={t("CTCT_LBL_CONTACTEMAIL")} placeholder={t("CTCT_LBL_CONTACTEMAIL")}
                        value={this.state.mail}
                        onChange={this.handleInputForm.bind(this, 'mail')}
                        type='email'
                    />
                </Form.Group>
                <Form.TextArea required label={t("CTCT_LBL_CONTACTMESSAGE")} placeholder={t("CTCT_LBL_CONTACTMESSAGE") + '...'}
                    value={this.state.message}
                    onChange={this.handleInputForm.bind(this, 'message')}
                />
            </Fragment>
        }


        return (
            <Modal trigger={triggerModal} open={this.state.open}>
                <Modal.Header>{t("CTCT_BTN_CONTACT")}</Modal.Header>
                <Modal.Content>
                    <Form>{formBody}</Form>
                    <div style={{textAlign: 'center'}}>
                        <Captcha validationPassedCallback={this.validationCaptchaPassed.bind(this)} />
                    </div>
                    <Message error hidden={this.state.error.hidden}>
                        <Message.Header>ERROR!!!</Message.Header>
                        {this.state.error.text}
                    </Message>
                    <Message positive hidden={this.state.returnMessage.hidden}>
                        <Message.Header>SUCCESS!!!</Message.Header>
                        {this.state.returnMessage.text}
                    </Message>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary onClick={this.checkData.bind(this)} loading={this.state.loading}>{t("CTCT_BTN_SEND")}</Button>
                    <Button negative onClick={this.openCloseModal.bind(this)} loading={this.state.loading}>{t("MAIN_BTN_CLOSE")}</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default translate('translations')(ContactForm)
